from selenium import webdriver
import time

driver = webdriver.Chrome()

driver.get('https://stackoverflow.com/questions/218384/what-is-a-nullpointerexception-and-how-do-i-fix-it/218510#218510')

time.sleep(2)

accept_button = driver.find_element('css selector', '.js-consent-banner .js-accept-cookies')
accept_button.click()

time.sleep(2)

driver.get('https://stackoverflow.com/questions/3988788/what-is-a-stack-trace-and-how-can-i-use-it-to-debug-my-application-errors')

time.sleep(5)
driver.quit()

