from selenium import webdriver
import time

driver = webdriver.Chrome()

driver.get('https://getbootstrap.com/docs/4.0/components/modal/')

launch_button = driver.find_element('xpath', '//button[contains(text(), "Launch demo modal")]')
launch_button.click()

time.sleep(2)

text_element = driver.find_element('css selector', '#exampleModalLive .modal-body')
print("-------  ", text_element.text)

close_button = driver.find_element('css selector', '#exampleModalLive .btn-secondary')
close_button.click()

time.sleep(5)
driver.quit()

